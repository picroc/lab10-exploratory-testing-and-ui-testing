import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestGoogle {

    @Test
    public void testGoogle() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","/Users/picroc/Downloads/yandexdriver");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("no-sandbox");
        options.addArguments("user-data-dir=~/Library/Application Support/Yandex/YandexBrowser");
        options.merge(capabilities);
        options.setBinary("/Applications/Yandex.app/Contents/MacOS/Yandex");
        WebDriver driver = new ChromeDriver(options);

        driver.get("https://www.spotify.com/us/account/overview/");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals(driver.getTitle(), "Account overview - Spotify");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/ul[1]/li[3]")));
        WebElement menuLink = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/ul[1]/li[3]"));
        menuLink.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/main[1]/section[1]/section[1]/h1")));
        WebElement pageText = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/main[1]/section[1]/section[1]/h1"));
        Assert.assertEquals(pageText.getAttribute("innerHTML"), "Manage your Family plan");
        menuLink = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/main[1]/div[2]/section[1]/a[1]"));
        menuLink.click();
        wait.until(ExpectedConditions.titleIs("Family Mix - Spotify"));
        Assert.assertEquals(driver.getTitle(), "Family Mix - Spotify");
        driver.quit();
    }
}
